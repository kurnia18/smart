﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class VGetSellThruSales
    {
        public int SellThruSalesId { get; set; }
        public string DistributorId { get; set; }
        public string SaleCode { get; set; }
        public double? Month { get; set; }
        public double? NetAmount { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
