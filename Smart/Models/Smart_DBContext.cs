﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Smart.Models
{
    public partial class Smart_DBContext : DbContext
    {
        public virtual DbSet<CaptureGetDailyReport> CaptureGetDailyReport { get; set; }
        public virtual DbSet<CaptureGetSellInBgtr> CaptureGetSellInBgtr { get; set; }
        public virtual DbSet<DashboardEffReachSummary> DashboardEffReachSummary { get; set; }
        public virtual DbSet<DashboardSellThruSummary> DashboardSellThruSummary { get; set; }
        public virtual DbSet<EffReachDetail> EffReachDetail { get; set; }
        public virtual DbSet<Hirarki> Hirarki { get; set; }
        public virtual DbSet<Target> Target { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<VGetArmonitoring> VGetArmonitoring { get; set; }
        public virtual DbSet<VGetCoverageSales> VGetCoverageSales { get; set; }
        public virtual DbSet<VGetDropSize> VGetDropSize { get; set; }
        public virtual DbSet<VGetSellThruSales> VGetSellThruSales { get; set; }
        public virtual DbSet<VGetSkuperStore> VGetSkuperStore { get; set; }

        public Smart_DBContext(DbContextOptions<Smart_DBContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CaptureGetDailyReport>(entity =>
            {
                entity.HasKey(e => e.DailyReportId);

                entity.Property(e => e.DailyReportId).HasColumnName("dailyReportID");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.SalesMtd).HasColumnName("SalesMTD");

                entity.Property(e => e.StockLm).HasColumnName("StockLM");

                entity.Property(e => e.StockMtd).HasColumnName("StockMTD");

                entity.Property(e => e.Swllm).HasColumnName("SWLLM");

                entity.Property(e => e.Swlmtd).HasColumnName("SWLMTD");

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.CaptureGetDailyReport)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_CaptureGetDailyReport");
            });

            modelBuilder.Entity<CaptureGetSellInBgtr>(entity =>
            {
                entity.HasKey(e => e.SellInBgtrid);

                entity.ToTable("CaptureGetSellInBGTR");

                entity.Property(e => e.SellInBgtrid).HasColumnName("SellInBGTRId");

                entity.Property(e => e.Bgcode)
                    .HasColumnName("BGCode")
                    .HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.CaptureGetSellInBgtr)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_CaptureGetSellInBGTR");
            });

            modelBuilder.Entity<DashboardEffReachSummary>(entity =>
            {
                entity.HasKey(e => e.EffReachSummaryId);

                entity.ToTable("Dashboard_EffReachSummary");

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Month).HasColumnName("MONTH");

                entity.Property(e => e.SaleCode).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.DashboardEffReachSummary)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_EffReachSummary");
            });

            modelBuilder.Entity<DashboardSellThruSummary>(entity =>
            {
                entity.HasKey(e => e.SellThruId);

                entity.ToTable("Dashboard_SellThruSummary");

                entity.Property(e => e.SellThruId).HasColumnName("sellThruId");

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Region).HasMaxLength(255);

                entity.Property(e => e.Tipe).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.DashboardSellThruSummary)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_SellThruSummary");
            });

            modelBuilder.Entity<EffReachDetail>(entity =>
            {
                entity.Property(e => e.Category).HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Tipe).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.EffReachDetail)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_EffReachDetail");
            });

            modelBuilder.Entity<Hirarki>(entity =>
            {
                entity.HasKey(e => e.DistributorId);

                entity.Property(e => e.DistributorId)
                    .HasColumnName("DistributorID")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Bpaddress)
                    .HasColumnName("BPAddress")
                    .HasMaxLength(255);

                entity.Property(e => e.Bpname)
                    .HasColumnName("BPName")
                    .HasMaxLength(255);

                entity.Property(e => e.ChannelCode).HasMaxLength(255);

                entity.Property(e => e.ChannelName).HasMaxLength(255);

                entity.Property(e => e.CountryCode).HasMaxLength(255);

                entity.Property(e => e.CountryName).HasMaxLength(255);

                entity.Property(e => e.EarliestData).HasMaxLength(255);

                entity.Property(e => e.EarliestDataStock).HasMaxLength(255);

                entity.Property(e => e.GroupCode).HasMaxLength(255);

                entity.Property(e => e.GroupName).HasMaxLength(255);

                entity.Property(e => e.LatestData).HasColumnType("datetime");

                entity.Property(e => e.LatestDataStock).HasColumnType("datetime");

                entity.Property(e => e.RegionCode).HasMaxLength(255);

                entity.Property(e => e.RegionName).HasMaxLength(255);

                entity.Property(e => e.RegionalCode).HasMaxLength(255);

                entity.Property(e => e.RowOrder).HasMaxLength(255);
            });

            modelBuilder.Entity<Target>(entity =>
            {
                entity.Property(e => e.TargetId).HasColumnName("targetId");

                entity.Property(e => e.Bpname)
                    .HasColumnName("BPName")
                    .HasMaxLength(255);

                entity.Property(e => e.Category).HasMaxLength(255);

                entity.Property(e => e.Channel).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.DistributorId)
                    .HasColumnName("DistributorID")
                    .HasMaxLength(255);

                entity.Property(e => e.Group).HasMaxLength(255);

                entity.Property(e => e.Region).HasMaxLength(255);

                entity.Property(e => e.ReportName).HasMaxLength(255);

                entity.Property(e => e.Target1).HasColumnName("Target");

                entity.Property(e => e.Type).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.Target)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorID_Target");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.AccountId);

                entity.Property(e => e.AccountId).HasColumnName("accountId");

                entity.Property(e => e.ActyId).HasColumnName("ACTY_ID");

                entity.Property(e => e.BanTimeDay)
                    .HasColumnName("banTimeDay")
                    .HasMaxLength(255);

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.IsOnline).HasColumnName("isOnline");

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LastUpdateStatus).HasMaxLength(255);

                entity.Property(e => e.LevelIndex).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255);

                entity.Property(e => e.PlntId)
                    .HasColumnName("PLNT_ID")
                    .HasMaxLength(255);

                entity.Property(e => e.Soldto)
                    .HasColumnName("soldto")
                    .HasMaxLength(255);

                entity.Property(e => e.UserLevel).HasMaxLength(255);
            });

            modelBuilder.Entity<VGetArmonitoring>(entity =>
            {
                entity.HasKey(e => e.ArmonitoringId);

                entity.ToTable("vGetARMonitoring");

                entity.Property(e => e.ArmonitoringId).HasColumnName("ARMonitoringId");

                entity.Property(e => e.Channel).HasMaxLength(255);

                entity.Property(e => e.Cluster).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Month).HasColumnName("MONTH");

                entity.Property(e => e.Region).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.VGetArmonitoring)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_vGetARMonitoring");
            });

            modelBuilder.Entity<VGetCoverageSales>(entity =>
            {
                entity.HasKey(e => e.CoverageSalesId);

                entity.ToTable("vGetCoverageSales");

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.SaleCode).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.VGetCoverageSales)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_vGetCoverageSales");
            });

            modelBuilder.Entity<VGetDropSize>(entity =>
            {
                entity.HasKey(e => e.DropSizeId);

                entity.ToTable("vGetDropSize");

                entity.Property(e => e.Channel).HasMaxLength(255);

                entity.Property(e => e.Cluster).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Month).HasColumnName("MONTH");

                entity.Property(e => e.Region).HasMaxLength(255);

                entity.Property(e => e.StoreType).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.VGetDropSize)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_vGetDropSize");
            });

            modelBuilder.Entity<VGetSellThruSales>(entity =>
            {
                entity.HasKey(e => e.SellThruSalesId);

                entity.ToTable("vGetSellThruSales");

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Month).HasColumnName("MONTH");

                entity.Property(e => e.SaleCode).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.VGetSellThruSales)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_vGetSellThruSales");
            });

            modelBuilder.Entity<VGetSkuperStore>(entity =>
            {
                entity.HasKey(e => e.SkuperStoreId);

                entity.ToTable("vGetSKUPerStore");

                entity.Property(e => e.SkuperStoreId).HasColumnName("SKUPerStoreId");

                entity.Property(e => e.Channel).HasMaxLength(255);

                entity.Property(e => e.Cluster).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(255);

                entity.Property(e => e.DistributorId).HasMaxLength(255);

                entity.Property(e => e.Month).HasColumnName("MONTH");

                entity.Property(e => e.Region).HasMaxLength(255);

                entity.Property(e => e.SkuperStore).HasColumnName("SKUPerStore");

                entity.Property(e => e.StoreType).HasMaxLength(255);

                entity.HasOne(d => d.Distributor)
                    .WithMany(p => p.VGetSkuperStore)
                    .HasForeignKey(d => d.DistributorId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_DistributorId_vGetSKUPerStore");
            });
        }
    }
}
