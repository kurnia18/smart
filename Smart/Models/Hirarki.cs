﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Hirarki
    {
        public Hirarki()
        {
            CaptureGetDailyReport = new HashSet<CaptureGetDailyReport>();
            CaptureGetSellInBgtr = new HashSet<CaptureGetSellInBgtr>();
            DashboardEffReachSummary = new HashSet<DashboardEffReachSummary>();
            DashboardSellThruSummary = new HashSet<DashboardSellThruSummary>();
            EffReachDetail = new HashSet<EffReachDetail>();
            Target = new HashSet<Target>();
            VGetArmonitoring = new HashSet<VGetArmonitoring>();
            VGetCoverageSales = new HashSet<VGetCoverageSales>();
            VGetDropSize = new HashSet<VGetDropSize>();
            VGetSellThruSales = new HashSet<VGetSellThruSales>();
            VGetSkuperStore = new HashSet<VGetSkuperStore>();
        }

        public double? CompanyId { get; set; }
        public string DistributorId { get; set; }
        public string Bpname { get; set; }
        public string CountryCode { get; set; }
        public string ChannelCode { get; set; }
        public string GroupCode { get; set; }
        public string RegionCode { get; set; }
        public string Bpaddress { get; set; }
        public string RowOrder { get; set; }
        public DateTime? LatestData { get; set; }
        public DateTime? LatestDataStock { get; set; }
        public string EarliestData { get; set; }
        public string EarliestDataStock { get; set; }
        public string RegionalCode { get; set; }
        public string RegionName { get; set; }
        public string GroupName { get; set; }
        public string ChannelName { get; set; }
        public string CountryName { get; set; }

        public ICollection<CaptureGetDailyReport> CaptureGetDailyReport { get; set; }
        public ICollection<CaptureGetSellInBgtr> CaptureGetSellInBgtr { get; set; }
        public ICollection<DashboardEffReachSummary> DashboardEffReachSummary { get; set; }
        public ICollection<DashboardSellThruSummary> DashboardSellThruSummary { get; set; }
        public ICollection<EffReachDetail> EffReachDetail { get; set; }
        public ICollection<Target> Target { get; set; }
        public ICollection<VGetArmonitoring> VGetArmonitoring { get; set; }
        public ICollection<VGetCoverageSales> VGetCoverageSales { get; set; }
        public ICollection<VGetDropSize> VGetDropSize { get; set; }
        public ICollection<VGetSellThruSales> VGetSellThruSales { get; set; }
        public ICollection<VGetSkuperStore> VGetSkuperStore { get; set; }
    }
}
