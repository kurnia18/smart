﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class User
    {
        public double AccountId { get; set; }
        public double? ActyId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
        public DateTime? LastLogin { get; set; }
        public double? Status { get; set; }
        public string LastUpdateStatus { get; set; }
        public string BanTimeDay { get; set; }
        public string PlntId { get; set; }
        public double? IsOnline { get; set; }
        public string Soldto { get; set; }
        public string UserLevel { get; set; }
        public string LevelIndex { get; set; }
    }
}
