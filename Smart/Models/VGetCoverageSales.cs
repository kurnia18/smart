﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class VGetCoverageSales
    {
        public int CoverageSalesId { get; set; }
        public string DistributorId { get; set; }
        public string SaleCode { get; set; }
        public double? Coverage { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
