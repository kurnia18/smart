﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class EffReachDetail
    {
        public int EffReachDetailId { get; set; }
        public string DistributorId { get; set; }
        public double? Month { get; set; }
        public string Category { get; set; }
        public string Tipe { get; set; }
        public double? Amount { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
