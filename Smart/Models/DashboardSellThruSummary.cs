﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class DashboardSellThruSummary
    {
        public int SellThruId { get; set; }
        public string Region { get; set; }
        public string DistributorId { get; set; }
        public double? Month { get; set; }
        public string Tipe { get; set; }
        public double? Ach { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
