﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class VGetArmonitoring
    {
        public int ArmonitoringId { get; set; }
        public string Country { get; set; }
        public string Channel { get; set; }
        public string Cluster { get; set; }
        public string Region { get; set; }
        public string DistributorId { get; set; }
        public double? Month { get; set; }
        public double? Overdue { get; set; }
        public double? PaymentPlan { get; set; }
        public double? Actual { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
