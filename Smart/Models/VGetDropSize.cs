﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class VGetDropSize
    {
        public int DropSizeId { get; set; }
        public string Country { get; set; }
        public string Channel { get; set; }
        public string Cluster { get; set; }
        public string Region { get; set; }
        public string DistributorId { get; set; }
        public string StoreType { get; set; }
        public double? Month { get; set; }
        public double? DropSizePerStore { get; set; }
        public double? NoOfStore { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
