﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class Target
    {
        public int TargetId { get; set; }
        public string DistributorId { get; set; }
        public string Bpname { get; set; }
        public string Country { get; set; }
        public string Channel { get; set; }
        public string Group { get; set; }
        public string Region { get; set; }
        public string ReportName { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public double? Month { get; set; }
        public double? Target1 { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
