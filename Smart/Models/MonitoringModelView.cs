﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smart.Models
{
    public class MonitoringModelView
    {
        public string selectionId { get; set; }
        public string areaId { get; set; }
        public string monthId { get; set; }
        public double sellThru { get; set; }
        public double sellThruTarget { get; set; }
        public double percentageSellThru { get; set; }
        public double percentageSellThruWidth { get; set; }
        public string sellThruGap { get; set; }

        public List<SalesAndStockList> SalesStockList { get; set; }
    }
}
