﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class DashboardEffReachSummary
    {
        public int EffReachSummaryId { get; set; }
        public string DistributorId { get; set; }
        public string SaleCode { get; set; }
        public double? Month { get; set; }
        public double? EffReach { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
