﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smart.Models
{
    public class KPIModelView
    {
        public string selectionId { get; set; }
        public string areaId { get; set; }
        public string monthId { get; set; }

        public double sellThru { get; set; }
        public double sellThruTarget { get; set; }
        public double percentageSellThru { get; set; }
        public double percentageSellThruWidth { get; set; }
        public string sellThruGap { get; set; }

        public string pick { get; set; }
        public string select { get; set; }
        public string area { get; set; }
        public string areaSelect { get; set; }
        public string month { get; set; }

        public List<KPILists> KPIList { get; set; }
        public List<EffReachLists> EffReachLists { get; set; }
    }
    public class KPILists
    {
        public string nameSellThru { get; set; }
        public double sumSellThru { get; set; }
        public double targetSellThru { get; set; }
        public double topSellThru { get; set; }
        public double nontopSellThru { get; set; }
        public double percentageSellThru { get; set; }
        public double topwidthSellThru { get; set; }
        public double nontopwidthSellThru { get; set; }
        public double toppercentageSellThru { get; set; }
        public double nontoppercentageSellThru { get; set; }
        public string gapSellThru { get; set; }
        public string styleBarSellThru { get; set; }
        public string styleTopSellThru { get; set; }
        public string styleNonSellThru { get; set; }

        
    }
    public class EffReachLists
    {
        public string nameEffReach { get; set; }
        public double effReachAct { get; set; }
        public double effReachPercentage { get; set; }
        public double effReachTarget { get; set; }
        public double effReachMax { get; set; }
        public string effReachGap { get; set; }
        public string styleEffReach { get; set; }
    }
}
