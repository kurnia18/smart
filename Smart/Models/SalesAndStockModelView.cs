﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smart.Models
{
    public class SalesAndStockModelView
    {
        public string selectionId { get; set; }
        public string areaId { get; set; }
        public string monthId { get; set; }
        public double sellThru { get; set; }
        public double sellThruTarget { get; set; }
        public double percentageSellThru { get; set; }
        public double percentageSellThruWidth { get; set; }
        public string sellThruGap { get; set; }

        public List<SalesAndStockList> SalesStockList { get; set; }
    }
    public class SalesAndStockList
    {
        public string nameSellThru { get; set; }
        public double sumSellThru { get; set; }
        public double targetSellThru { get; set; }
        public double topSellThru { get; set; }
        public double nontopSellThru { get; set; }
        public double percentageSellThru { get; set; }
        public double topwidthSellThru { get; set; }
        public double nontopwidthSellThru { get; set; }
        public double toppercentageSellThru { get; set; }
        public double nontoppercentageSellThru { get; set; }
        public string gapSellThru { get; set; }
        public string styleBarSellThru { get; set; }
        public string styleTopSellThru { get; set; }
        public string styleNonSellThru { get; set; }

        public string nameSellIn { get; set; }
        public double sumSellIn { get; set; }
        public double targetSellIn { get; set; }
        public double percentageSellIn { get; set; }
        public string gapSellIn { get; set; }
        public string styleBarSellIn { get; set; }
    } 
}
