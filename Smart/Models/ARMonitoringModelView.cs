﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Smart.Models
{
    public class ARMonitoringModelView
    {
        public string selectionId { get; set; }
        public string areaId { get; set; }
        public string monthId { get; set; }

        public List<ARMonitoringList> ARMonitoringList { get; set; }
    }
    public class ARMonitoringList
    {
        public double sumDue { get; set; }
        public double sumPaymentPlan { get; set; }
        public double sumActual { get; set; }
    }
}
