﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class CaptureGetSellInBgtr
    {
        public int SellInBgtrid { get; set; }
        public string Bgcode { get; set; }
        public double? Value { get; set; }
        public double? Month { get; set; }
        public string DistributorId { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
