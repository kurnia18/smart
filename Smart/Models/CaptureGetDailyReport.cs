﻿using System;
using System.Collections.Generic;

namespace Smart.Models
{
    public partial class CaptureGetDailyReport
    {
        public int DailyReportId { get; set; }
        public string Description { get; set; }
        public double? SalesMtd { get; set; }
        public double? StockMtd { get; set; }
        public double? Swlmtd { get; set; }
        public double? StockLm { get; set; }
        public double? Swllm { get; set; }
        public string DistributorId { get; set; }
        public double? Month { get; set; }
        public double? SalesTarget { get; set; }
        public double? SalesTarget2 { get; set; }

        public Hirarki Distributor { get; set; }
    }
}
