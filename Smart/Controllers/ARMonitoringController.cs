﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Smart.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;

namespace Smart.Controllers
{
    public class ARMonitoringController : Controller
    {
        private readonly Smart_DBContext _context;

        public ARMonitoringController(Smart_DBContext context)
        {

            _context = context;
        }

        public void listSales()
        {
            List<SelectListItem> selection = new List<SelectListItem>();
            selection.Add(new SelectListItem { Text = "--- Select Region ---", Value = "0" });
            selection.Add(new SelectListItem { Text = "Country", Value = "country" });
            selection.Add(new SelectListItem { Text = "Channel", Value = "channel" });
            selection.Add(new SelectListItem { Text = "Cluster", Value = "cluster" });
            selection.Add(new SelectListItem { Text = "Region", Value = "region" });
            selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            ViewData["regional"] = selection;

            int month_min = System.Convert.ToInt32(_context.VGetArmonitoring.Min(x => x.Month));
            int month_max = System.Convert.ToInt32(_context.VGetArmonitoring.Max(x => x.Month));
            List<SelectListItem> monthSelect = new List<SelectListItem>();
            monthSelect.Add(new SelectListItem { Text = "--- Select Month ---", Value = "0" });
            for (int i = month_min; i <= month_max; i++)
            {
                monthSelect.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            ViewData["month"] = monthSelect;
        }

        [HttpGet]
        public IActionResult Index()
        {
            listSales();
            return View();
        }

        [HttpPost]
        public ActionResult Index(ARMonitoringModelView model)
        {
            listSales();

            string id = model.selectionId;
            string areaId = model.areaId;
            double monthId = System.Convert.ToDouble(model.monthId);

            var ar = new ARMonitoringModelView();
            ar.ARMonitoringList = new List<ARMonitoringList>();

            double sumDue; 
            double sumPaymentPlan;
            double sumActual;
            double totalSumDue = 0;
            double totalSumPaymentPlan = 0;
            double totalSumActual = 0;

            var arList = new List<string>();

            if (id == "country")
            {
                arList = _context.Hirarki.Where(w => w.CountryName.Contains(areaId)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();

            }
            else if (id == "channel")
            {
                arList = _context.Hirarki.Where(w => w.ChannelName.Contains(areaId)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "cluster")
            {
                arList = _context.Hirarki.Where(w => w.GroupName.Contains(areaId)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "region")
            {
                arList = _context.Hirarki.Where(w => w.RegionName.Contains(areaId)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "territory")
            {
                arList = _context.Hirarki.Where(w => w.DistributorId.Contains(areaId)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else
            {
                arList = null;
            }

            foreach (string item in arList)
            {
                sumDue = _context.VGetArmonitoring.Where(x => x.DistributorId == item && x.Month == monthId).Select(y => y.Overdue ?? 0).Sum();
                totalSumDue += sumDue;

                sumPaymentPlan = _context.VGetArmonitoring.Where(x => x.DistributorId == item && x.Month == monthId).Select(y => y.PaymentPlan ?? 0).Sum();
                totalSumPaymentPlan += sumPaymentPlan;

                sumActual = _context.VGetArmonitoring.Where(x => x.DistributorId == item && x.Month == monthId).Select(y => y.Actual ?? 0).Sum();
                totalSumActual += sumActual;
            }

            ViewData["totalSumDue"] = System.Math.Round(totalSumDue);
            ViewData["totalSumPaymentPlan"] = System.Math.Round(totalSumPaymentPlan);
            ViewData["totalSumActual"] = System.Math.Round(totalSumActual);

            ViewData["selected"] = id;
            ViewData["areaId"] = areaId;
            ViewData["monthId"] = monthId;

            return View();
        }

        public JsonResult GetArea(string id)
        {

            List<SelectListItem> area = new List<SelectListItem>();

            var listCountryId = _context.Hirarki.GroupBy(x => x.CountryCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountryName = _context.Hirarki.GroupBy(x => x.CountryName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountry = listCountryId.Zip(listCountryName, (v, t) => new { Value = v, Text = t });

            var listChannelId = _context.Hirarki.GroupBy(x => x.ChannelCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannelName = _context.Hirarki.GroupBy(x => x.ChannelName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannel = listChannelId.Zip(listChannelName, (v, t) => new { Value = v, Text = t });

            var listClusterId = _context.Hirarki.GroupBy(x => x.GroupCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listClusterName = _context.Hirarki.GroupBy(x => x.GroupName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCluster = listClusterId.Zip(listClusterName, (v, t) => new { Value = v, Text = t });

            var listRegionId = _context.Hirarki.GroupBy(x => x.RegionCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegionName = _context.Hirarki.GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegion = listRegionId.Zip(listRegionName, (v, t) => new { Value = v, Text = t });

            var listTeritoryId = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritoryName = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritory = listTeritoryId.Zip(listTeritoryName, (v, t) => new { Value = v, Text = t });

            switch (id)
            {
                case "0":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });
                    break;
                case "country":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCountry)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "channel":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listChannel)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "cluster":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCluster)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "region":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listRegion)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "territory":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listTeritory)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
            }
            return Json(new SelectList(area, "Value", "Text"));
        }
    }
}