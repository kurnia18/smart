﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Smart.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;

namespace Smart.Controllers
{
    public class HomeController : Controller
    {
        private readonly Smart_DBContext _context;

        public HomeController(Smart_DBContext context)
        {
            
            _context = context;
        }

        public ActionResult Index()
        {
            var _userLogged = HttpContext.Session.GetString("userLogged");
            if (_userLogged == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                jsonSellThru();
                jsonSellIn();
                jsonStock();
                return View();
            }

        }

        public void jsonSellThru()
        {
            int month_max = System.Convert.ToInt16((_context.CaptureGetDailyReport.Max(x => x.Month)) - 201700);
            int month_min = System.Convert.ToInt16((_context.CaptureGetDailyReport.Min(x => x.Month)) - 201700);
            double[] amount = new double[month_max + 1];
            double[] targetAmount = new double[month_max + 1];

            List<SellTrhu> sellthrus = new List<SellTrhu>();

            for (int i = month_min; i <= month_max; i++)
            {
                SellTrhu sellthru = new SellTrhu();
                double[] month = new double[month_max + 1];
                month[i] = 201700 + i;
                amount[i] = ((_context.CaptureGetDailyReport.Where(y => y.Month == month[i]).Select(x => x.SalesMtd ?? 0).Sum()) / 1000);
                targetAmount[i] = ((_context.CaptureGetDailyReport.Where(y => y.Month == month[i]).Select(x => x.SalesTarget ?? 0).Sum()) / 1000);

                sellthru.month = System.Convert.ToInt32(month[i]);
                sellthru.value = System.Convert.ToInt32(amount[i]);
                sellthru.target = System.Convert.ToInt32(targetAmount[i]);
                sellthrus.Add(sellthru);
            }

            string json1 = JsonConvert.SerializeObject(sellthrus);
            ViewData["json1"] = json1;
        }

        public void jsonSellIn()
        {
            int month_max = System.Convert.ToInt16((_context.CaptureGetSellInBgtr.Max(x => x.Month)) - 201700);
            double[] amount = new double[month_max + 1];
            double[] targetAmount = new double[month_max + 1];

            ViewData["month_max2"] = month_max;
            List<SellIn> sellIns = new List<SellIn>();

            for (int i = 1; i <= month_max; i++)
            {
                SellIn sellin = new SellIn();
                double[] months = new double[month_max + 1];
                months[i] = 201700 + i;
                amount[i] = ((_context.CaptureGetSellInBgtr.Where(y => y.Month == months[i]).Select(x => x.Value ?? 0).Sum()) / 1000);
                targetAmount[i] = ((_context.Target.Where(y => y.Month == months[i] && y.Type == "SellIn").Select(x => x.Target1 ?? 0).Sum()) / 1000);

                sellin.month = System.Convert.ToInt32(months[i]);
                sellin.value = System.Convert.ToInt32(amount[i]);
                sellin.target = System.Convert.ToInt32(targetAmount[i]);
                sellIns.Add(sellin);
            }

            string json2 = JsonConvert.SerializeObject(sellIns);
            ViewData["json2"] = json2;
        }

        public void jsonStock()
        {
            int month_max = System.Convert.ToInt16((_context.CaptureGetDailyReport.Max(x => x.Month)) - 201700);
            int month_min = System.Convert.ToInt16((_context.CaptureGetDailyReport.Min(x => x.Month)) - 201700);
            double[] _stock = new double[month_max + 1];
            double[] value = new double[month_max + 1];

            ViewData["month_max2"] = month_max;
            List<Stock> stocks = new List<Stock>();

            for (int i = month_min; i <= month_max; i++)
            {
                Stock stock = new Stock();
                double[] months = new double[month_max + 1];
                months[i] = 201700 + i;
                _stock[i] = ((_context.CaptureGetDailyReport.Where(y => y.Month == months[i]).Select(x => x.StockMtd ?? 0).Sum()) / 1000);
                value[i] = ((_context.CaptureGetDailyReport.Where(y => y.Month == months[i]).Select(x => x.Swlmtd ?? 0).Sum()) / 1000);

                stock.month = System.Convert.ToInt32(months[i]);
                stock.stock = System.Convert.ToInt32(_stock[i]);
                stock.value = System.Convert.ToInt32(value[i]);
                stocks.Add(stock);
            }

            string json3 = JsonConvert.SerializeObject(stocks);
            ViewData["json3"] = json3;
        }
        
        public IActionResult ProcessKPI()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult MarketingPromoReport()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public async Task<IActionResult> ARMonitoringAsync()
        {
            var smart_dbContext = _context.VGetArmonitoring.Include(v => v.Distributor);
            return View(await smart_dbContext.ToListAsync());
            
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    internal class SellTrhu
    {
        public int month { get; set; }
        public int value { get; set; }
        public int target { get; set; }
    }
    internal class SellIn
    {
        public int month { get; set; }
        public int value { get; set; }
        public int target { get; set; }
    }
    internal class Stock
    {
        public int month { get; set; }
        public int stock { get; set; }
        public int value { get; set; }
    }
}
