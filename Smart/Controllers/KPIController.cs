﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Smart.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;

namespace Smart.Controllers
{
    public class KPIController : Controller
    {
        private readonly Smart_DBContext _context;

        public KPIController(Smart_DBContext context)
        {

            _context = context;
        }

        public void listSales()
        {
            string level = HttpContext.Session.GetString("userLevel");
            List<SelectListItem> selection = new List<SelectListItem>();
            selection.Add(new SelectListItem { Text = "--- Select Region ---", Value = "0" });
            if (level == "Regional" || level == "Country")
            {
                selection.Add(new SelectListItem { Text = "Country", Value = "country" });
                selection.Add(new SelectListItem { Text = "Channel", Value = "channel" });
                selection.Add(new SelectListItem { Text = "Cluster", Value = "cluster" });
                selection.Add(new SelectListItem { Text = "Region", Value = "region" });
                selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            }
            else if (level == "Channel")
            {
                selection.Add(new SelectListItem { Text = "Channel", Value = "channel" });
                selection.Add(new SelectListItem { Text = "Cluster", Value = "cluster" });
                selection.Add(new SelectListItem { Text = "Region", Value = "region" });
                selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            }
            else if (level == "Cluster")
            {
                selection.Add(new SelectListItem { Text = "Cluster", Value = "cluster" });
                selection.Add(new SelectListItem { Text = "Region", Value = "region" });
                selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            }
            else if (level == "Region")
            {
                selection.Add(new SelectListItem { Text = "Region", Value = "region" });
                selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            }
            else
            {
                selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            }
            ViewData["regional"] = selection;

            int month_min = System.Convert.ToInt32(_context.DashboardSellThruSummary.Min(x => x.Month));
            int month_max = System.Convert.ToInt32(_context.DashboardSellThruSummary.Max(x => x.Month));
            List<SelectListItem> monthSelect = new List<SelectListItem>();
            monthSelect.Add(new SelectListItem { Text = "--- Select Month ---", Value = "0" });
            for (int i = month_min; i <= month_max; i++)
            {
                monthSelect.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            ViewData["month"] = monthSelect;
        }

        [HttpGet]
        public IActionResult Index()
        {
            listSales();
            return View();
        }

        [HttpPost]
        public ActionResult Index(KPIModelView model)
        {
            listSales();

            string pick = HttpContext.Request.Form["pick"];
            string select = HttpContext.Request.Form["select"];
            string area = HttpContext.Request.Form["area"];
            string areaSelect = HttpContext.Request.Form["areaSelect"];
            string month = HttpContext.Request.Form["month"];

            model.pick = pick;
            model.select = select;
            model.area = area;
            model.areaSelect = area;
            model.month = month;

            var effReach = new KPIModelView();
            effReach.EffReachLists = new List<EffReachLists>();

            string id;
            string areaId;
            double monthId;
            if (pick != null)
            {
                id = select;
                areaId = area;
                monthId = System.Convert.ToDouble(month);
                ViewBag.Message = id + " "+ areaId+ " "+monthId;

                double effReachAct;
                double effReachTarget;
                double effReachPercentage;
                double _effReachMax; double effReachMax = 0;
                string effReachGap;
                string styleEffReach;

                var effReachList = new List<string>();
                var effReachListItem = new List<string>();
                effReachList = _context.Hirarki.Where(w => w.RegionName.Contains(areaSelect)).GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
                foreach (var item in effReachList)
                {
                    effReachListItem = _context.EffReachDetail.Where(w => w.DistributorId == item).GroupBy(x => x.Category).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
                    _effReachMax = System.Math.Round(_context.EffReachDetail.Where(x => x.Month == monthId && x.DistributorId.Contains(item) && x.Tipe != "Act").Select(y => y.Amount ?? 0).Sum(), 2);
                    effReachMax += _effReachMax;
                }
                foreach (var items in effReachListItem)
                {
                    effReachAct = System.Math.Round(_context.EffReachDetail.Where(x => x.Month == monthId && x.Category.Contains(items) && x.Tipe != "Act").Select(y => y.Amount ?? 0).Sum(), 2);
                    effReachTarget = System.Math.Round(_context.EffReachDetail.Where(x => x.Month == monthId && x.Category.Contains(items) && x.Tipe != "Target").Select(y => y.Amount ?? 0).Sum(), 2);
                    effReachPercentage = effReachAct - effReachTarget;
                    if (effReachPercentage > 0)
                    {
                        styleEffReach = "green";
                        effReachGap = "Over :" + effReachPercentage;
                    }
                    else
                    {
                        styleEffReach = "red";
                        effReachGap = "Gap :" + effReachPercentage;
                    }
                    effReach.EffReachLists.Add(new EffReachLists
                    {
                        nameEffReach = items,
                        effReachAct = effReachAct,
                        effReachTarget = effReachTarget,
                        effReachPercentage = effReachPercentage,
                        effReachGap = effReachGap,
                        styleEffReach = styleEffReach
                    });
                }
                ViewData["effReachMax"] = effReachMax;
            }
            else
            {
                id = model.selectionId;
                areaId = model.areaId;
                monthId = System.Convert.ToDouble(model.monthId);
            }
            
            var summaries = new KPIModelView();
            summaries.KPIList = new List<KPILists>();

            double sumSummarySellThru; double percentageSummarySellThru;
            double targetSummarySellThru; double topWidthSellThru;
            double topSummarySellThru; double nonTopWidthSellThru;
            double nonTopSummarySellThru; double nonTopPercentageSellThru;
            double topPercentageSellThru; string summaryGapSellThru;
            string barSellThru, topSellThru, nonSellThru;

            var summaryList = new List<string>();
            double sellTrhuTarget;
            double sellTrhu = 0;

            if (id == "country")
            {
                sellTrhuTarget = _context.Target.Where(x => x.Month == monthId && x.Country.Contains(areaId) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum();
                summaryList = _context.Hirarki.Where(w => w.CountryName.Contains(areaId)).GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();

            }
            else if (id == "channel")
            {
                sellTrhuTarget = _context.Target.Where(x => x.Month == monthId && x.Channel.Contains(areaId) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum();
                summaryList = _context.Hirarki.Where(w => w.ChannelName.Contains(areaId)).GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "cluster")
            {
                sellTrhuTarget = _context.Target.Where(x => x.Month == monthId && x.Group.Contains(areaId) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum();
                summaryList = _context.Hirarki.Where(w => w.GroupName.Contains(areaId)).GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "region")
            {
                sellTrhuTarget = _context.Target.Where(x => x.Month == monthId && x.Region.Contains(areaId) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum();
                summaryList = _context.Hirarki.Where(w => w.RegionName.Contains(areaId)).GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else if (id == "territory")
            {
                sellTrhuTarget = _context.Target.Where(x => x.Month == monthId && x.DistributorId.Contains(areaId) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum();
                summaryList = _context.Hirarki.Where(w => w.DistributorId.Contains(areaId)).GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            }
            else
            {
                sellTrhuTarget = 0;
                summaryList = null;
            }

            foreach (string item in summaryList)
            {
                //kelola data sellthru
                sumSummarySellThru = System.Math.Round(_context.DashboardSellThruSummary.Where(x => x.Month == monthId && x.Region.Contains(item) && x.Tipe != "Target").Select(y => y.Ach ?? 0).Sum(), 2);
                sellTrhu += sumSummarySellThru;

                if (item.Contains("Central"))
                {
                    targetSummarySellThru = System.Math.Round(_context.Target.Where(x => x.Month == monthId && x.Region.Contains("Central") && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum(), 2);
                }
                else if (item.Contains("East"))
                {
                    targetSummarySellThru = System.Math.Round(_context.Target.Where(x => x.Month == monthId && x.Region.Contains("East") && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum(), 2);
                }
                else if (item.Contains("West"))
                {
                    targetSummarySellThru = System.Math.Round(_context.Target.Where(x => x.Month == monthId && x.Region.Contains("West") && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum(), 2);
                }
                else
                {
                    targetSummarySellThru = System.Math.Round(_context.Target.Where(x => x.Month == monthId && x.Region.Contains(item) && x.Type == "SellThru").Select(y => y.Target1 ?? 0).Sum(), 2);
                }

                topSummarySellThru = System.Math.Round(_context.DashboardSellThruSummary.Where(x => x.Month == monthId && x.Region.Contains(item) && x.Tipe == "Top").Select(y => y.Ach ?? 0).Sum(), 2);
                nonTopSummarySellThru = System.Math.Round(_context.DashboardSellThruSummary.Where(x => x.Month == monthId && x.Region.Contains(item) && x.Tipe == "NonTop").Select(y => y.Ach ?? 0).Sum(), 2);
                percentageSummarySellThru = System.Math.Round((sumSummarySellThru * 100) / targetSummarySellThru);
                if (percentageSummarySellThru > 0)
                {
                    nonTopPercentageSellThru = System.Math.Round((100 * nonTopSummarySellThru) / topSummarySellThru, 2);
                    topPercentageSellThru = System.Math.Round((100 * topSummarySellThru) / nonTopSummarySellThru, 2);
                    if (nonTopPercentageSellThru > 100) nonTopPercentageSellThru = System.Math.Round((100 - topPercentageSellThru), 2);
                    if (topPercentageSellThru > 100) topPercentageSellThru = System.Math.Round((100 - nonTopPercentageSellThru), 2);
                }
                else
                {
                    topPercentageSellThru = 0;
                    nonTopPercentageSellThru = 0;
                }
                topWidthSellThru = System.Math.Round(((percentageSummarySellThru / ((percentageSummarySellThru + 100) / 100)) * topPercentageSellThru) / 100);
                nonTopWidthSellThru = System.Math.Round(((percentageSummarySellThru / ((percentageSummarySellThru + 100) / 100)) * nonTopPercentageSellThru) / 100);
                double gap1 = System.Math.Round((sumSummarySellThru / 1000000000) - (targetSummarySellThru / 1000000000), 2);
                if (gap1 > 0)
                {
                    summaryGapSellThru = "Over :" + gap1 + " bio of " + System.Math.Round(targetSummarySellThru / 1000000000, 2) + " bio";
                }
                else
                {
                    summaryGapSellThru = "Gap :" + gap1 + " bio of " + System.Math.Round(targetSummarySellThru / 1000000000, 2) + " bio";
                }
                if (percentageSummarySellThru >= 100.00)
                {
                    barSellThru = "green"; topSellThru = "#157c0c"; nonSellThru = "#35af2a";
                }
                else if (percentageSummarySellThru < 90.00)
                {
                    barSellThru = "red"; topSellThru = "#c91e12"; nonSellThru = "#ff0000";
                }
                else
                {
                    barSellThru = "#ef8f00"; topSellThru = "#ef8f00"; nonSellThru = "#e0e01a";
                }
                summaries.KPIList.Add(new KPILists()
                {
                    nameSellThru = item,
                    sumSellThru = sumSummarySellThru,
                    targetSellThru = targetSummarySellThru,
                    topSellThru = topSummarySellThru,
                    nontopSellThru = nonTopSummarySellThru,
                    percentageSellThru = percentageSummarySellThru,
                    topwidthSellThru = topWidthSellThru,
                    nontopwidthSellThru = nonTopWidthSellThru,
                    toppercentageSellThru = topPercentageSellThru,
                    nontoppercentageSellThru = nonTopPercentageSellThru,
                    gapSellThru = summaryGapSellThru,
                    styleBarSellThru = barSellThru,
                    styleTopSellThru = topSellThru,
                    styleNonSellThru = nonSellThru
                });
            }

            double percentageSellThru = System.Math.Round((sellTrhu * 100) / sellTrhuTarget, 2);
            double percentageSellThruWidth = System.Math.Round(percentageSellThru / ((percentageSellThru + 100) / 100));
            string sellThruGap = "Over :" + System.Math.Round((sellTrhu / 1000000000) - (sellTrhuTarget / 1000000000), 2) + " bio of " + System.Math.Round(sellTrhuTarget / 1000000000, 2) + " bio";

            ViewData["sellThru"] = sellTrhu;
            ViewData["sellThruTarget"] = sellTrhuTarget;
            ViewData["percentage"] = percentageSellThru;
            ViewData["percentageWidth"] = percentageSellThruWidth;
            ViewData["sellThruGap"] = sellThruGap;

            ViewData["selected"] = id;
            ViewData["areaId"] = areaId;
            ViewData["monthId"] = monthId;

            model.sellThru = sellTrhu;
            model.sellThruTarget = sellTrhuTarget;
            model.percentageSellThru = percentageSellThru;
            model.percentageSellThruWidth = percentageSellThruWidth;
            model.sellThruGap = sellThruGap;

            var modelView = new KPIModelView();
            modelView.KPIList = summaries.KPIList;
            modelView.EffReachLists = effReach.EffReachLists;

            if (pick != null)
            {
                return View(modelView);
            }
            else
            {
                return View(summaries);
            }
            //}
        }

        public JsonResult GetArea(string id)
        {
            List<SelectListItem> area = new List<SelectListItem>();

            var listCountryId = _context.Hirarki.GroupBy(x => x.CountryCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountryName = _context.Hirarki.GroupBy(x => x.CountryName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountry = listCountryId.Zip(listCountryName, (v, t) => new { Value = v, Text = t });

            var listChannelId = _context.Hirarki.GroupBy(x => x.ChannelCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannelName = _context.Hirarki.GroupBy(x => x.ChannelName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannel = listChannelId.Zip(listChannelName, (v, t) => new { Value = v, Text = t });

            var listClusterId = _context.Hirarki.GroupBy(x => x.GroupCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listClusterName = _context.Hirarki.GroupBy(x => x.GroupName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCluster = listClusterId.Zip(listClusterName, (v, t) => new { Value = v, Text = t });

            var listRegionId = _context.Hirarki.GroupBy(x => x.RegionCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegionName = _context.Hirarki.GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegion = listRegionId.Zip(listRegionName, (v, t) => new { Value = v, Text = t });

            var listTeritoryId = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritoryName = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritory = listTeritoryId.Zip(listTeritoryName, (v, t) => new { Value = v, Text = t });

            switch (id)
            {
                case "0":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });
                    break;
                case "country":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCountry)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "channel":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listChannel)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "cluster":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCluster)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "region":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listRegion)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "territory":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listTeritory)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
            }
            return Json(new SelectList(area, "Value", "Text"));
        }

        [HttpPost]
        public ActionResult Load(KPIModelView model)
        {
            string pick = HttpContext.Request.Form["pick"];
            string select = HttpContext.Request.Form["select"];
            string area = HttpContext.Request.Form["area"];
            string month = HttpContext.Request.Form["month"];

            model.pick = pick;
            model.select = select;
            model.area = area;
            model.month = month;

            return RedirectToAction("Index", "KPI");
        }
    }
}