﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Smart.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System;

namespace Smart.Controllers
{
    public class MarketingController : Controller
    {
        private readonly Smart_DBContext _context;

        public MarketingController(Smart_DBContext context)
        {

            _context = context;
        }

        public void listSales()
        {
            List<SelectListItem> selection = new List<SelectListItem>();
            selection.Add(new SelectListItem { Text = "--- Select Region ---", Value = "0" });
            selection.Add(new SelectListItem { Text = "Country", Value = "country" });
            selection.Add(new SelectListItem { Text = "Channel", Value = "channel" });
            selection.Add(new SelectListItem { Text = "Cluster", Value = "cluster" });
            selection.Add(new SelectListItem { Text = "Region", Value = "region" });
            selection.Add(new SelectListItem { Text = "Territory", Value = "territory" });
            ViewData["regional"] = selection;

            int month_min = System.Convert.ToInt32(_context.DashboardSellThruSummary.Min(x => x.Month));
            int month_max = System.Convert.ToInt32(_context.DashboardSellThruSummary.Max(x => x.Month));
            List<SelectListItem> monthSelect = new List<SelectListItem>();
            monthSelect.Add(new SelectListItem { Text = "--- Select Month ---", Value = "0" });
            for (int i = month_min; i <= month_max; i++)
            {
                monthSelect.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            ViewData["month"] = monthSelect;
        }

        [HttpGet]
        public IActionResult Index()
        {
            listSales();
            return View();
        }

        [HttpPost]
        public ActionResult Index(ARMonitoringModelView model)
        {
            listSales();
            return View();
        }

        public JsonResult GetArea(string id)
        {
            List<SelectListItem> area = new List<SelectListItem>();

            var listCountryId = _context.Hirarki.GroupBy(x => x.CountryCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountryName = _context.Hirarki.GroupBy(x => x.CountryName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCountry = listCountryId.Zip(listCountryName, (v, t) => new { Value = v, Text = t });

            var listChannelId = _context.Hirarki.GroupBy(x => x.ChannelCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannelName = _context.Hirarki.GroupBy(x => x.ChannelName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listChannel = listChannelId.Zip(listChannelName, (v, t) => new { Value = v, Text = t });

            var listClusterId = _context.Hirarki.GroupBy(x => x.GroupCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listClusterName = _context.Hirarki.GroupBy(x => x.GroupName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listCluster = listClusterId.Zip(listClusterName, (v, t) => new { Value = v, Text = t });

            var listRegionId = _context.Hirarki.GroupBy(x => x.RegionCode).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegionName = _context.Hirarki.GroupBy(x => x.RegionName).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listRegion = listRegionId.Zip(listRegionName, (v, t) => new { Value = v, Text = t });

            var listTeritoryId = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritoryName = _context.Hirarki.GroupBy(x => x.DistributorId).Where(y => y.Count() >= 1).Select(z => z.Key).ToList();
            var listTeritory = listTeritoryId.Zip(listTeritoryName, (v, t) => new { Value = v, Text = t });

            switch (id)
            {
                case "0":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });
                    break;
                case "country":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCountry)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "channel":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listChannel)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "cluster":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listCluster)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "region":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listRegion)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
                case "territory":
                    area.Add(new SelectListItem { Text = "-- Select Area --", Value = "0" });

                    foreach (var item in listTeritory)
                    {
                        area.Add(new SelectListItem { Text = item.Text, Value = item.Text });
                    }
                    break;
            }
            return Json(new SelectList(area, "Value", "Text"));
        }
    }
}