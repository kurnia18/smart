﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Smart.Models;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Smart.Controllers
{
    public class AccountController : Controller
    {
        private readonly Smart_DBContext _context;

        public AccountController(Smart_DBContext context)
        {
            _context = context;
        }

        [RequireHttps]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(User model)
        {

            var email = model.Email;
            var password = MD5Hash(model.Password);

            if (_context.User.Any(x => x.Email == email))
            {
                ViewData["userEmail"] = "User email ada";
                if (_context.User.Any(x => x.Email == email))
                {
                    if (_context.User.Any(x => x.Password == password))
                    {
                        HttpContext.Session.SetString("userLogged", "logged");
                        var level = _context.User.Where(x => x.Email == email).Single().UserLevel.ToString();
                        var nama = _context.User.Where(x => x.Email == email).Single().Email.ToString();
                        ViewData["name"] = nama;
                        HttpContext.Session.SetString("userLevel", level);
                        HttpContext.Session.SetString("userName", nama);
                        string name = HttpContext.Session.GetString("userName");

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewData["userEmail"] = "Password tidak cocok";
                        return View();
                    }
                }
            }
            else
            {
                ViewData["userEmail"] = "User email tidak ada";
                return View();
            }

            return View();
        }

        public static string MD5Hash(string input)
        {
            byte[] originalBytes;
            byte[] encodedBytes;
            MD5 md5;

            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(input);
            encodedBytes = md5.ComputeHash(originalBytes);

            string base64 = System.Convert.ToBase64String(encodedBytes);

            return base64;
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}